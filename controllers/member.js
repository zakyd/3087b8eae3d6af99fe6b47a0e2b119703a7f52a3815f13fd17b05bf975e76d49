const mysql = require('../libraries/mysql');

async function getTree(name) {
    const member = {
        name,
        children: [],
    };
    const result = await mysql('SELECT * FROM member WHERE name = ? LIMIT 1', [name]);
    if (result && result.length) {
        const children = await mysql('SELECT * FROM member WHERE parent_id = ?', [result[0].id]);
        // eslint-disable-next-line no-restricted-syntax
        for (const child of children || []) {
            if (child.id !== result[0].id) {
                // eslint-disable-next-line no-await-in-loop
                member.children.push(await getTree(child.name));
            }
        }
    } else {
        return {};
    }
    return member;
}

async function getParents(name) {
    const result = await mysql('SELECT * FROM member WHERE name = ? LIMIT 1', [name]);
    const parents = [];
    if (result && result.length) {
        let member = result[0];
        let parentId = member.parent_id;
        while (member.id !== parentId) {
            // eslint-disable-next-line no-await-in-loop
            const result2 = await mysql('SELECT * FROM member WHERE id = ? LIMIT 1', [parentId]);
            if (result2 && result2.length) {
                [member] = result2;
                parentId = member.parent_id;
                parents.push(member.name);
            } else {
                break;
            }
        }
    }
    return parents;
}

async function getChildren(name) {
    const result = await mysql('SELECT * FROM member WHERE name = ? LIMIT 1', [name]);
    if (result && result.length) {
        const member = result[0];
        const children = await mysql('SELECT * FROM member WHERE parent_id = ? AND id != ?', [member.id, member.id]);
        return children.map((each) => each.name);
    }
    return [];
}

module.exports = {
    getTree,
    getParents,
    getChildren,
};
