# MKMPRG2021

## Requirements

- NPM v6
- NodeJS v14

## How To Use

- Clone this repository
- `cd project-name`
- `npm install`
- `cp configs/.env.example configs/.env`
- Configure your environment at `configs/.env`
- Run `npm start`
- Open your browser, then visit `http://localhost:3000/member/:name` (Example: `http://localhost:3000/member/root`, `http://localhost:3000/member/William`)