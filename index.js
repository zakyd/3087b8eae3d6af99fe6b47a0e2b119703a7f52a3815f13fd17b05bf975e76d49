const express = require('express');

const app = express();
const port = 3000;

const member = require('./controllers/member');

app.get('/member/:name?', async (req, res) => {
    const name = req.params.name || 'root';
    const tree = await member.getTree(name);
    const parents = await member.getParents(name);
    const children = await member.getChildren(name);
    return res.json({
        tree,
        parents,
        children,
    });
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});

function end() {
    setTimeout(() => {
        process.exit();
    }, 1000);
}

process.on('SIGINT', () => {
    end();
});
