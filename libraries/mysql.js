const mysql = require('mysql2');

const env = require('../env');

const pool = mysql.createPool({
    host: env.DB_MS_HOST,
    user: env.DB_MS_USERNAME,
    password: env.DB_MS_PASSWORD,
    database: env.DB_MS_DATABASE,
    port: env.DB_MS_PORT,
});

const query = (...sql) => new Promise((resolve, reject) => {
    pool.query(...sql, (err, result) => {
        if (err) reject(err);
        resolve(result);
    });
});

process.on('SIGINT', async () => {
    await pool.end();
});

module.exports = query;
